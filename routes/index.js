var express = require('express');
var router = express.Router();
var player_dal = require('../model/player_dal');
var playername = 'none';
/* GET home page. */
router.get('/', function(req, res, next) {
    player_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('index', {'result': result, title: 'SIFAC Card Tracker'});
        }
    });
});

router.get('/about', function(req, res, next) {
    res.render('about', {title: 'About'});
});

module.exports = router;
