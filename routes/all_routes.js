var express = require('express');
var router = express.Router();
var player_dal = require('../model/player_dal');
var all_dal = require('../model/all_dal');


// View All accounts
router.get('/all', function(req, res) {
    account_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', { 'result':result });
        }
    });

});

// View the account for the given id
router.get('/', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        player_dal.getPlayer(req.query.player_id, function(err, result){
            res.render('home', {'title': 'hello', 'player': result[0]});
        });

        }
    }
);

router.get('/allCards', function(req, res){
        if(req.query.player_id == null) {
            res.send('player_id is null');
        }
        else {
            player_dal.getPlayer(req.query.player_id, function(err, result){
                res.render('all/allCards', {'title': 'hello', 'player': result[0]});
            });

        }
    }
);

// Print out all the player's cards
router.get('/allMemberCards', function(req, res){
    player_dal.getPlayer(req.query.player_id, function(err2, result2){
        all_dal.getMemberCards(req.query.player_id, req.query.idol_name, req.query.rarity_name, req.query.series_name, function(err,result,count) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('all/allMemberCards', {'result': result, 'count': count[0], 'player': result2[0]});
            }
        });
    });
});

router.get('/allMemberCardsOptions', function(req, res){
    player_dal.getPlayer(req.query.player_id, function(err2, result2){
        player_dal.getDatabaseInfo(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('all/allMemberCardsOptions', {idol: result[0], skill: result[1], rarity: result[2], subunit: result[3], series: result[4], 'player': result2[0]});
            }
        });
    });
});

router.get('/allSkillCards', function(req, res){
    player_dal.getPlayer(req.query.player_id, function(err2, result2){
        all_dal.getSkillCards(req.query.player_id, req.query.idol_name, req.query.skill_name, function(err,result,count) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('all/allSkillCards', {'result': result, 'count': count[0], 'player': result2[0]});
            }
        });
    });
});

router.get('/allSkillCardsOptions', function(req, res){
    player_dal.getPlayer(req.query.player_id, function(err2, result2){
        player_dal.getDatabaseInfo(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('all/allSkillCardsOptions', {idol: result[0], skill: result[1], rarity: result[2], subunit: result[3], series: result[4], 'player': result2[0]});
            }
        });
    });
});

// Return the add a new account form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    account_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('account/accountAdd', {'account': result});
        }
    });
});

// View the account for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.email == "") {
        res.send('email name must be provided.');
    }
    else if(req.query.first_name == "") {
        res.send('first_name must be provided.');
    }
    else if(req.query.last_name == "") {
        res.send('last_name must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        account_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/account/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.account_id == null) {
        res.send('A account id is required');
    }
    else {
        account_dal.edit(req.query.account_id, function(err, result){
            res.render('account/accountUpdate', {account: result[0][0]});
        });
    }

});

/*router.get('/edit2', function(req, res){
   if(req.query.account_id == null) {
       res.send('A account id is required');
   }
   else {
       account_dal.getById(req.query.account_id, function(err, account){
           account_dal.getAll(function(err, account) {
               res.render('account/accountUpdate', {account: account[0], account: account});
           });
       });
   }

});*/

router.get('/update', function(req, res) {
    account_dal.update(req.query, function(err, result){
       res.redirect(302, '/account/all');
    });
});

// Delete a account for the given account_id
router.get('/delete', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
         account_dal.delete(req.query.account_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/account/all');
             }
         });
    }
});

module.exports = router;
