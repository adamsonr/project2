var express = require('express');
var router = express.Router();
var player_dal = require('../model/player_dal');


// View All accounts
router.get('/all', function(req, res) {
    account_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', { 'result':result });
        }
    });

});

router.get('/newPlayer', function(req, res) {
    res.render('player/newPlayer');

});

router.get('/addNew', function(req, res) {
    player_dal.insertPlayer(req.query.player_name, req.query.player_level, function(err, result){
        player_dal.getAll(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('index', {'result': result, title: 'SIFAC Card Tracker', was_successful:true});
            }
        });
    })

});

// View the account for the given id
router.get('/', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        player_dal.getPlayer(req.query.player_id, function(err, result){
            res.render('home', {'title': 'SIFAC Card Tracker', 'player': result[0]});
        });

        }
    }
);

router.get('/myCards', function(req, res){
        if(req.query.player_id == null) {
            res.send('player_id is null');
        }
        else {
            player_dal.getPlayer(req.query.player_id, function(err, result){
                res.render('player/myCards', {'title': 'hello', 'player': result[0]});
            });

        }
    }
);

// Print out all the player's cards
router.get('/myMemberCards', function(req, res){
    player_dal.getPlayer(req.query.player_id, function(err2, result2){
        player_dal.getMemberCards(req.query.player_id, req.query.idol_name, req.query.rarity_name, req.query.series_name, function(err,result,count) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/myMemberCards', {'result': result, 'count': count[0], 'player': result2[0]});
            }
        });
    });
});
router.get('/myMemberCardsOptions', function(req, res){
    player_dal.getPlayer(req.query.player_id, function(err2, result2){
        player_dal.getDatabaseInfo(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/myMemberCardsOptions', {idol: result[0], skill: result[1], rarity: result[2], subunit: result[3], series: result[4], 'player': result2[0]});
            }
        });
    });
});

router.get('/mySkillCards', function(req, res){
    player_dal.getPlayer(req.query.player_id, function(err2, result2){
        player_dal.getSkillCards(req.query.player_id, req.query.idol_name, req.query.skill_name, req.query.level, function(err,result, count) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/mySkillCards', {'result': result, 'count': count[0], 'player': result2[0]});
            }
        });
    });
});

router.get('/mySkillCardsOptions', function(req, res){
    player_dal.getPlayer(req.query.player_id, function(err2, result2){
        player_dal.getDatabaseInfo(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/mySkillCardsOptions', {idol: result[0], skill: result[1], rarity: result[2], subunit: result[3], series: result[4], 'player': result2[0]});
            }
        });
    });
});

router.get('/addMemberCardOptions', function(req, res){
    player_dal.getPlayer(req.query.player_id, function(err2, result2){
        player_dal.getDatabaseInfo(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/addMemberCardOptions', {idol: result[0], skill: result[1], rarity: result[2], subunit: result[3], series: result[4], 'player': result2[0]});
            }
        });
    });
});

router.get('/addSkillCardOptions', function(req, res){
    player_dal.getPlayer(req.query.player_id, function(err2, result2){
        player_dal.getDatabaseInfo(function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/addSkillCardOptions', {idol: result[0], skill: result[1], rarity: result[2], subunit: result[3], series: result[4], 'player': result2[0]});
            }
        });
    });
});

router.get('/addCard', function(req, res){
        if(req.query.player_id == null) {
            res.send('player_id is null');
        }
        else {
            player_dal.getPlayer(req.query.player_id, function(err, result){
                res.render('player/addCardSelectType', {'title': 'hello', 'player': result[0]});
            });

        }
    }
);


router.get('/addMemberCard', function(req, res){
    // simple validation
    if(req.query.idol_name == "") {
        res.send('idol name must be provided.');
    }
    else if(req.query.rarity_name == "") {
        res.send('rarity_name must be provided.');
    }
    else if(req.query.series_name == "") {
        res.send('series_name must be provided.');
    }
    else {
        player_dal.getPlayer(req.query.player_id, function(err3, result3){
            player_dal.getMemberCardID(req.query, function(err2, result2){
                player_dal.insertMemberCard(req.query.player_id, result2[0], function(err,result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('player/addCardSelectType', {'player': result3[0], was_successful: true});
                    }
                });
            });
        });
    }
});

router.get('/addSkillCard', function(req, res){
    // simple validation
    if(req.query.idol_name == "") {
        res.send('idol name must be provided.');
    }
    else if(req.query.skill_name == "") {
        res.send('skill_name must be provided.');
    }
    else if(req.query.level == "") {
        res.send('level must be provided.');
    }
    else {
        player_dal.getPlayer(req.query.player_id, function(err3, result3){
            player_dal.getSkillCardID(req.query, function(err2, result2){
                player_dal.insertSkillCard(req.query.player_id, result2[0], req.query.skill_level, function(err,result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('player/addCardSelectType', {'player': result3[0], was_successful: true});
                    }
                });
            });
        });
    }
});

router.get('/deleteMemberCard', function(req, res){
    player_dal.getPlayer(req.query.player_id, function(err2, result2){
        player_dal.deleteMemberCard(req.query.player_id, req.query.card_id, function(err,result3) {
            if (err) {
                res.send(err);
            }
            else {
                player_dal.getDatabaseInfo(function(err,result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('player/myMemberCardsOptions', {idol: result[0], skill: result[1], rarity: result[2], subunit: result[3], series: result[4], 'player': result2[0], was_successful: true});
                    }
                });
            }
        });
    });
});

router.get('/deleteSkillCard', function(req, res){
    player_dal.getPlayer(req.query.player_id, function(err2, result2){
        player_dal.deleteSkillCard(req.query.player_id, req.query.card_id, function(err,result3) {
            if (err) {
                res.send(err);
            }
            else {
                player_dal.getDatabaseInfo(function(err,result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('player/mySkillCardsOptions', {idol: result[0], skill: result[1], rarity: result[2], subunit: result[3], series: result[4], 'player': result2[0], was_successful: true});
                    }
                });
            }
        });
    });
});

router.get('/editSkillCard', function(req, res){
    player_dal.getPlayer(req.query.player_id, function(err2, result2){
        player_dal.editSkillCard(req.query.player_id, req.query.card_id, req.query.skill_level, function(err,result3) {
            if (err) {
                res.send(err);
            }
            else {
                player_dal.getDatabaseInfo(function(err,result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('player/mySkillCardsOptions', {idol: result[0], skill: result[1], rarity: result[2], subunit: result[3], series: result[4], 'player': result2[0], was_edited: true});
                    }
                });
            }
        });
    });
});

/*router.get('/edit2', function(req, res){
   if(req.query.account_id == null) {
       res.send('A account id is required');
   }
   else {
       account_dal.getById(req.query.account_id, function(err, account){
           account_dal.getAll(function(err, account) {
               res.render('account/accountUpdate', {account: account[0], account: account});
           });
       });
   }

});*/

router.get('/update', function(req, res) {
    account_dal.update(req.query, function(err, result){
       res.redirect(302, '/account/all');
    });
});

// Delete a account for the given account_id
router.get('/delete', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
         account_dal.delete(req.query.account_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/account/all');
             }
         });
    }
});

module.exports = router;
