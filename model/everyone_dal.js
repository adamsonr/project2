var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM player ORDER BY player_name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });


};

exports.getById = function(account_id, callback) {
    var query = 'SELECT * from account ' +
        'WHERE account_id = ?';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.getPlayer = function(player_id, callback) {
    var query = 'SELECT * from player ' +
        'WHERE player_id = ?';
    var queryData = [player_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.getMemberCards = function(player_id, idol_name, rarity_name, series_name, callback) {
    var query = 'SELECT * from all_member_cards_count ' +
        '       where 1=1 ';
    var queryData = [];
    if(idol_name != "all") {
        if (idol_name == "BiBi" || idol_name == "Printemps" || idol_name == "lily white") {
            queryData.push(idol_name);
            query += " and subunit_name = ? ";
        }
        else{
            queryData.push(idol_name);
            query += " and idol_name = ? ";
        }
    }

    if(rarity_name != "all"){
        queryData.push(rarity_name);
        query += " and rarity_name = ? ";
    }

    if(series_name != "all"){
        queryData.push(series_name);
        query += " and series_name = ? ";
    }

    query += " order by card_id";
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        query2 = "SELECT sum(cards.count) as count from (" + query + ") as cards";
        console.log(query2);
        connection.query(query2, queryData, function(err, count){
            callback(err, result, count);
        });
    });
};

exports.getSkillCards = function(player_id, idol_name, skill_name, callback) {
    var query = 'SELECT * from all_skill_cards_count ' +
        'where 1=1 ';
    var queryData = [];
    if(idol_name != "all") {
        if (idol_name == "BiBi" || idol_name == "Printemps" || idol_name == "lily white") {
            queryData.push(idol_name);
            query += " and subunit_name = ? ";
        }
        else{
            queryData.push(idol_name);
            query += " and idol_name = ? ";
        }
    }

    if(skill_name != "all"){
        queryData.push(skill_name);
        query += " and skill_name = ? ";
    }

    query += " order by card_id";
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        query2 = "SELECT sum(cards.count) as count from (" + query + ") as cards";
        console.log(query2);
        connection.query(query2, queryData, function(err, count){
            callback(err, result, count);
        });
    });
};

exports.getDatabaseInfo = function(callback) {
    var query = 'CALL sifac_getDatabaseInfo();';
    console.log(query);

    connection.query(query, function(err, result) {

        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE account SET email = ?, first_name = ?, last_name = ? WHERE account_id = ?';
    var queryData = [params.email, params.first_name, params.last_name, params.account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
        });

};

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO account (email, first_name, last_name) VALUES (?, ?, ?)';

    var queryData = [params.email, params.first_name, params.last_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};